package ru.tsc.felofyanov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.felofyanov.tm.model.ProjectDTO;
import ru.tsc.felofyanov.tm.service.ProjectDTOService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private ProjectDTOService service;

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project")
            @RequestBody ProjectDTO project
    ) {
        service.remove(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "projects")
            @RequestBody List<ProjectDTO> projects
    ) {
        service.remove(projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        service.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return service.findAll().stream().collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDTO save(
            @WebParam(name = "project")
            @RequestBody ProjectDTO project
    ) {
        return service.save(project);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<ProjectDTO> saveAll(
            @WebParam(name = "projects")
            @RequestBody List<ProjectDTO> projects
    ) {
        return service.save(projects);
    }
}
