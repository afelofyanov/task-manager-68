package ru.tsc.felofyanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.felofyanov.tm.model.TaskDTO;

public interface ITaskDTORepository extends JpaRepository<TaskDTO, String> {

}
