package ru.tsc.felofyanov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.model.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.model.IProjectService;
import ru.tsc.felofyanov.tm.model.Project;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return projectRepository;
    }
}
