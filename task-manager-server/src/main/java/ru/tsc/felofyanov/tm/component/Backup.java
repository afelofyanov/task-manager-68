package ru.tsc.felofyanov.tm.component;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@AllArgsConstructor
public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private final Bootstrap bootstrap;

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3000, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    @SneakyThrows
    public void load() {
        bootstrap.getDomainService().loadDataBackup();
    }
}
