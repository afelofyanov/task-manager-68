package ru.tsc.felofyanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnerDTORepository<M extends AbstractWbsDTO> extends IDTORepository<M> {

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    @NotNull
    Page<M> findAllByUserId(@Nullable String userId, @NotNull Pageable pageable);

    void deleteByUserId(@NotNull String userId);

    @Nullable
    M findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    M deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);
}
