package ru.tsc.felofyanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.DataLoadYamlFasterXmlRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Component
public final class DataLoadYamlFasterXmlListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-load-yaml-fasterxml";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from yaml file(FasterXML)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataLoadYamlFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().loadDataYamlFasterXml(new DataLoadYamlFasterXmlRequest(getToken()));
    }
}
