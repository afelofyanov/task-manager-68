package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public final class AccessDeniedException extends AbstractExcception {

    public AccessDeniedException(Throwable cause) {
        super(cause);
    }

    public AccessDeniedException() {
        super("ERROR! Access denied....");
    }
}
