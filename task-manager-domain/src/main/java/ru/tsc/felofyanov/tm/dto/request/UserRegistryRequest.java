package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    @Nullable
    private Role role;

    public UserRegistryRequest(
            @Nullable String token,
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    ) {
        super(token);
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }
}
